#include <iostream>
#include <iomanip>
#include <random>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <vector>

using namespace std;

int main()
{
    //input size of matrix
    int n;
    //int n;
    cout << "Enter row count: ";
    cin >> n;
    cout << "Enter col count: ";
    cin >> n;
    cout << "\n";

    //create random generator between -20 and 20
    const float range_from = -20.00;
    const float range_to = 20.00;
    std::random_device                  rand_dev;
    std::mt19937                        generator(rand_dev());
    std::uniform_real_distribution<float>  distr(range_from, range_to);

    // create square matrix A
    float** A = new float* [n];

    // create array B
    std::vector<float> B;

    // initialize sum
    float sum = 0.00;

    // fill and output matrix
    for (int i = 0; i < n; ++i)
        A[i] = new float[n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            A[i][j] = distr(generator);
            std::cout << std::fixed << std::setw(6) << std::setprecision(2)
                << A[i][j] << " ";
        }
        cout << setw(2) << "\n";
    }

    //calculate sum
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            sum += A[i][j];
        }
    }

    //count positive numbers under main diagonal and calculate vector elements
    for (int i = 0; i < n; i++)
    {
        int count = 0;
        for (int j = 0; j < n; j++)
        {
            if (A[i][j] > 0)
            {
                if (i > j)
                {
                    count++;
                }
            }
        }
        B.push_back(count / sum);
    }

    //output sum
    cout << setw(2) << "\n";
    cout << setw(2) << "\n";
    cout << "sum: " << sum;
    cout << setw(2) << "\n";
    cout << setw(2) << "\n";

    //output vector b 
    cout << "count/sum: " << "\n";
    for (int x = 0; x < B.size(); x++)
    {
        cout << setw(6) << "B[" << x << "]: " << B[x] << " \n";
    }
    cout << setw(2) << "\n";

    cout << setw(2) << "\n";
    cout << setw(2) << "\n";
    cout << "Final: " << "\n";
    cout.precision(2); //������ ������ #.## � 2-�� ������� ���� ����
    for (int i = 0; i < n; i++) //������� ������ � 0-��� ����� �������
    {
        int count = 0;
        float max = A[i][0]; //�������� � 0 �������� �����.
        for (int j = 0; j < n; j++) {
            sum += A[i][j];
            if (A[i][j] > max) { max = A[i][j]; } //��������� ������������ �������
            if (A[i][j] > 0) { count++; }    //������, ���� ������� ��������
            if (i % 2 == 0) { B[i] = (count / max); }
            else { B[i] = i; } //���������� ����� � ������� �������� B
            cout << "B[" << j << "]: " << B[j] << " \n";
        }
    }
}